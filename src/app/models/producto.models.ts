export class Producto {

    nombre: string;
    imgUrl: string;
    precio: number;
    cantidad: number;
    departamento: string;

    constructor(pNombre: string ='', pImgUrl: string ='', pPrecio: number = 0, pCantidad: number = 0,pDepartamento: string ='') {
        this.nombre = pNombre;
        this.imgUrl = pImgUrl;
        this.precio = pPrecio;
        this.cantidad = pCantidad;
        this.departamento = pDepartamento;
    }

    
}