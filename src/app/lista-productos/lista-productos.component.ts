import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Producto } from '../models/producto.models';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {

  @Input() titulo: string;
  @Input() productos: Producto[];
  @Output() productoSeleccionado: EventEmitter<Producto>;

  @Input() textoBoton: string;
  @Output() productoSeleccionado2: EventEmitter<number>;


  
  constructor() { 
    this.titulo = 'asfdf'
    this.productos =[]
    this.productoSeleccionado = new EventEmitter();
    this.productoSeleccionado2 = new EventEmitter();
  }

  ngOnInit(): void {
  }

  onClick(producto: Producto){
    this.productoSeleccionado.emit(producto);
  }

  onClick2(indice: number){
    this.productoSeleccionado2.emit(indice);
  }

}
