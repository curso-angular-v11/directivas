import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Producto } from '../models/producto.models';
import { Tarea } from '../models/tarea.models';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  nuevaTarea: Tarea;

  @Output() tareaCreada: EventEmitter<Tarea>

  nuevoProducto: Producto;

  @Output() productoCreado: EventEmitter<Producto>

  constructor() { 
    this.nuevaTarea = new Tarea();
    this.tareaCreada = new EventEmitter();

    this.nuevoProducto= new Producto();
    this.productoCreado = new EventEmitter();


  }

  ngOnInit(): void {
  }

  onClick(){
    this.tareaCreada.emit(this.nuevaTarea);
    this.nuevaTarea = new Tarea();
  }

  onClick2(){
    this.productoCreado.emit(this.nuevoProducto);
    this.nuevoProducto = new Producto();
  }

}
