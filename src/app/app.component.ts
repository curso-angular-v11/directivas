import { Component } from '@angular/core';
import { Tarea } from './models/tarea.models';
import { Producto } from './models/producto.models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  propiedadesParrafo: any;
  mostrar: boolean;

  arrTareas: Tarea[];

  arrComida: Producto[];
  arrBebida: Producto[];
  productosSeleccionados: Producto[];

  productosComprados: Producto[];

  // productosListados:Producto[];

  constructor(){
    this.propiedadesParrafo ={
      color: 'red', 
      fontSize:'24px'
    }

    this.mostrar = true;

    this.arrTareas =[];

    this.arrComida =[
      new Producto('Cocido', 'http://i.blogs.es/31e3ce/cocido-madrileno/840_560.jpg', 10.50),
      new Producto('Paella', 'http://www.comedera.com/wp-content/uploads/2014/02/paella-de-mariscos-700x400.jpg', 12.20),
      new Producto('Cachopo', 'https://www.recetasderechupete.com/wp-content/uploads/2017/11/Cachopo-768x527.jpg', 15.75),
      new Producto('Pizza', 'http://www.laespanolaaceites.com/wp-content/uploads/2019/06/pizza-con-chorizo-jamon-y-queso-1080x671.jpg', 7.80),
      new Producto('Pasta', 'http://www.hola.com/imagenes//cocina/escuela/20180830129064/video-receta-como-hacer-pasta-cacera/0-595-236/pasta-casera-t.jpg', 6.90),
      new Producto('Hamburguesa', 'http://www.hogar.mapfre.es/media/2018/09/hamburguesa-sencilla.jpg', 5.75),
    ];
    this.arrBebida=[
      new Producto('Coca Cola', 'http://kalamazoo.es/content/images/product/38466_1_xnl.jpg', 1.75),
      new Producto('Fanta', 'http://supercostablanca.es/7430-thickbox_default/fanta-naranja-33cl.jpg', 1.65),
      new Producto('Cerveza', 'http://s.libertaddigital.com/2019/01/02/1920/1080/fit/cerveza-fresca.jpg', 15.75),
      new Producto('Agua', 'https://raja.scene7.com/is/image/Raja/products/vella-agua-mineral-natural-33-cl_36087.jpg?image=KAL_36087_1_xnl$nextpdt$', 1.50),
    ]

    this.productosSeleccionados = [];

    this.productosComprados = [];
  }

  cambiaColor(pColor: string){
    switch(pColor){
      case 'a':
        this.propiedadesParrafo.color = 'yellow';
        break;
      case 'r':
        this.propiedadesParrafo.color = 'red';
        break;
      case 'v':
        this.propiedadesParrafo.color = 'green';
        break;    
      }
  }

  onInput($event){
    this.propiedadesParrafo.fontSize =`${$event.target.value}px`;
  }

  onClickMostrar(){
    this.mostrar = !this.mostrar;
  }

  onTareaCreada($event){
    this.arrTareas.push($event);
  }

  onProductoSeleccionado($event){
    const productoEncontrado = this.productosSeleccionados.find(producto => producto.nombre === $event.nombre)

    if (productoEncontrado){
      productoEncontrado.cantidad++;
    }else{
      $event.cantidad = 1;
      this.productosSeleccionados.push($event); 
    }
    console.log(this.productosSeleccionados)
  }

  onProductoCreado($event){
    this.productosSeleccionados.push($event);
  }

  onProductoSeleccionado2($event){
    const prod = this.productosSeleccionados.splice($event,1);
    this.productosComprados.push(prod[0]);
  }

  onProductoNoSeleccionado2($event){
    const prod = this.productosComprados.splice($event,1);
    this.productosSeleccionados.push(prod[0]);
  }
}
