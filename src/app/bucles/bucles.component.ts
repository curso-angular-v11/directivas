import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bucles',
  templateUrl: './bucles.component.html',
  styleUrls: ['./bucles.component.css']
})
export class BuclesComponent implements OnInit {

  personas: any[];
  constructor() {
    this.personas = [
      {nombre:'Janice', apellido:'Najera Pabón', edad: 36, calle:'Ctra. de Siles, 16', telefono:'741 385 960'},
      {nombre:'Eliazar', apellido:'Estevéz Carmona', edad: 13, calle:'Avda. Rio Nalon, 7', telefono:'643 933 009'},
      {nombre:'Apolo', apellido:'Loya sierra', edad: 26, calle:'Plazuela do Porto', telefono:'783 326 373'},
      {nombre:'Ascla', apellido:'Hernandez Yañez', edad: 67, calle:'Salzillo, 60', telefono:'684 653 495'},
      {nombre:'Benet', apellido:'Betancourt Godoy', edad: 47, calle:'Cañadilla, 47 ', telefono:'785 664 317'},
      {nombre:'Winifreda', apellido:'Cedillo Leyva', edad: 33, calle:'Rua do Paseo, 99', telefono:'693 673 215'},
      {nombre:'Popea', apellido:'Lebrón Rangel', edad: 51, calle:'Reiseñor, 38', telefono:'754 637 272'},
    ];
   }

  ngOnInit(): void {
  }

  onClick(){
    this.personas.push({
      nombre: 'Matias', apellido:'Guerrero', calle:'Colombia, 36', telefono:'333 444 555'
    })
  }

}
